﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;
using WindowsDesktop;

namespace ViDIcon
{
    public partial class FormMain : Form
    {
        /// <summary>
        /// 仮想デスクトップの内容を表すアイコン
        /// </summary>
        private Dictionary<int, Icon> Icons { get; set; } = new Dictionary<int, Icon>();

        /// <summary>
        /// 仮想デスクトップ名
        /// </summary>
        private Dictionary<int, string> DesktopNames { get; set; } = new Dictionary<int, string>();

        /// <summary>
        /// 仮想デスクトップ
        /// </summary>
        private VirtualDesktop[] Desktops { get; set; }

        public FormMain()
        {
            InitializeComponent();

            Text = Application.ProductName;
            notifyIcon1.Text = Application.ProductName;

            menuItemReloadIcons.Click += MenuItemReloadIcons_Click;
            menuItemAbout.Click += MenuItemAbout_Click;
            menuItemExit.Click += MenuItemExit_Click;
        }

        /// <summary>
        /// デフォルトのデスクトップ名を返す
        /// </summary>
        private string DefaultDesktopName(int desktopNumber)
        {
            return $"Desktop {desktopNumber}";
        }

        /// <summary>
        /// 既定の場所に保存されたアイコンファイルを読み込む
        /// </summary>
        private void LoadIcons()
        {
            string exeFolderPath = Path.GetDirectoryName(Application.ExecutablePath);
            string resourcePath = Path.Combine(exeFolderPath, @"Resources\Icons");
            foreach (string path in Directory.GetFiles(resourcePath, "*.ico"))
            {
                char fileNumberSeparator = '_';
                string fileName = Path.GetFileNameWithoutExtension(path);
                int fileNumber;
                if (int.TryParse(fileName, out fileNumber))
                {
                    Icons[fileNumber] = new Icon(path);
                    DesktopNames[fileNumber] = DefaultDesktopName(fileNumber);
                }
                else if (fileName.Contains(fileNumberSeparator))
                {
                    string fileNumberPart = fileName.Split(fileNumberSeparator)[0];
                    if (int.TryParse(fileNumberPart, out fileNumber))
                    {
                        Icons[fileNumber] = new Icon(path);
                        DesktopNames[fileNumber] = string.Join(fileNumberSeparator, fileName.Split(fileNumberSeparator).Skip(1));
                    }
                }
            }
        }

        /// <summary>
        /// 現在の仮想デスクトップに対応したアイコンとデスクトップ名を、このフォームにセットする
        /// </summary>
        private void SetCurrentDesktopIcon()
        {
            if (!VirtualDesktop.IsPinnedWindow(Handle))
            {
                VirtualDesktop.PinWindow(Handle);
            }

            Desktops = VirtualDesktop.GetDesktops();
            for (int i = 0; i < Desktops.Length; i++)
            {
                int desktopNumber = i + 1;
                if (Desktops[i].Equals(VirtualDesktop.Current))
                {
                    // アイコンを変更
                    if (Icons.ContainsKey(desktopNumber))
                    {
                        Icon = Icons[desktopNumber];
                    }
                    else
                    {
                        Icon = Icons.First().Value;
                    }
                    BackgroundImage = Icon.ToBitmap();
                    notifyIcon1.Icon = Icon;

                    // テキストを変更
                    Text = GetDesktopName(desktopNumber);
                    notifyIcon1.Text = Text;

                    break;
                }
            }
        }

        /// <summary>
        /// デスクトップの名称を取得する
        /// </summary>
        /// <param name="desktopNumber">デスクトップの番号（ 1 で始まる）</param>
        /// <returns></returns>
        private string GetDesktopName(int desktopNumber)
        {
            if (DesktopNames.ContainsKey(desktopNumber))
            {
                return DesktopNames[desktopNumber];
            }
            else
            {
                return DefaultDesktopName(desktopNumber);
            }
        }

        private void VirtualDesktop_CurrentChanged(object sender, VirtualDesktopChangedEventArgs e)
        {
            SetCurrentDesktopIcon();
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            // コンテキストメニューを削除する
            foreach (var menuItem in contextMenuStrip1.Items.Cast<ToolStripItem>().Reverse())
            {
                if (menuItem is ToolStripMenuItemChangeCurrentDesktop)
                {
                    contextMenuStrip1.Items.Remove(menuItem as ToolStripMenuItem);
                }
            }

            // 現在の仮想デスクトップの分だけコンテキストメニューを追加する
            for (int i = 0; i < Desktops.Count(); i++)
            {
                int separator2Index = contextMenuStrip1.Items.IndexOf(toolStripSeparator2);
                var currentDesktop = Desktops[i];
                int currentDesktopNumber = i + 1;
                string desktopName = GetDesktopName(currentDesktopNumber);
                string contextMenuText = (currentDesktopNumber < 10 ? "&" : "") + $"{currentDesktopNumber}: [{desktopName}] に切り替え";
                var desktopMenuItem = new ToolStripMenuItemChangeCurrentDesktop(contextMenuText);
                desktopMenuItem.Click += (sender, e) => currentDesktop.Switch();
                contextMenuStrip1.Items.Insert(separator2Index, desktopMenuItem);
            }
        }

        private void MenuItemExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MenuItemAbout_Click(object sender, EventArgs e)
        {
            var formAbout = new FormAbout();
            formAbout.ShowDialog();
        }

        private void MenuItemReloadIcons_Click(object sender, EventArgs e)
        {
            LoadIcons();
            SetCurrentDesktopIcon();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            LoadIcons();

            Desktops = VirtualDesktop.GetDesktops();

            VirtualDesktop.CurrentChanged += VirtualDesktop_CurrentChanged;
        }

        private void FormMain_Shown(object sender, EventArgs e)
        {
            SetCurrentDesktopIcon();
        }
    }
}
